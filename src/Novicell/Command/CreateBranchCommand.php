<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell\Command;

use JsonException;
use Symfony\Component\Console\{
    Command\Command as SymfonyCommand,
    Helper\QuestionHelper,
    Input\InputArgument,
    Input\InputInterface,
    Input\InputOption,
    Output\OutputInterface,
    Question\Question
};
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Process\Process;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Throwable;

class CreateBranchCommand extends SymfonyCommand
{
    private const CONFIG_FILE = '.config' . DIRECTORY_SEPARATOR . 'novicell.json';
    private const JIRA_API_URL = 'https://novicell.atlassian.net/rest/api/latest/issue/';
    private const JIRA_API_TOKENS_LINK = 'https://id.atlassian.com/manage-profile/security/api-tokens';
    private const JIRA_FIELDS = 'summary';
    private const NOVICELL_EMAIL = '@novicell.dk';
    private string $configFile = '';
    protected static $defaultName = 'git:branch:create';

    protected function configure(): void
    {
        $this->configFile = (getenv('HOME') ?: '~') . DIRECTORY_SEPARATOR . self::CONFIG_FILE;
        $this->addArgument('task_id', InputArgument::OPTIONAL, 'Why didn\'t you paste a task id?', '');
        $this->addOption('title', 't', InputOption::VALUE_OPTIONAL, 'Task title?');
        $this->setHelp('If you need to edit credentials, edit or delete this file: ' . $this->configFile);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $questionHelper = new QuestionHelper();
        $configFile = $this->configFile;
        if (!file_exists($configFile)) {
            $userInitials = $questionHelper->ask($input, $output, new Question('Your initials: '));
            $userApiKey = $questionHelper->ask($input, $output, new Question('Your Atlassian API key (' . self::JIRA_API_TOKENS_LINK . '): '));
            if ($userInitials && $userApiKey) {
                try {
                    file_put_contents(
                        $configFile,
                        json_encode(
                            [
                                'username' => $userInitials . self::NOVICELL_EMAIL,
                                'password' => $userApiKey
                            ],
                            JSON_THROW_ON_ERROR
                        )
                    );
                } catch (JsonException $e) {
                    $output->writeln('Could not read config file.');

                    return SymfonyCommand::FAILURE;
                }
            }
        }
        try {
            $configFileContents = json_decode(file_get_contents($configFile), false, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $output->writeln('Could not read config file.');

            return SymfonyCommand::FAILURE;
        }
        $taskId = $input->getArgument('task_id');
        while (!preg_match('/^(\p{L})+(-)+(\d)+$/u', $taskId)) {
            if ($taskId) {
                $output->writeln('Wrong task id format, try again (xyz-123)');
            }
            $taskId = $questionHelper->ask($input, $output, new Question('Task ID: '));
        }
        $taskId = strtoupper($taskId);
        $taskTitle = $input->getOption('title');
        if (!$taskTitle) {
            $httpClient = HttpClient::create();
            $response = $httpClient->request(
                'GET',
                self::JIRA_API_URL . $taskId . '?fields=' . self::JIRA_FIELDS,
                [
                    'headers' => [
                        'Accept: application/json',
                        'Authorization: Basic ' . base64_encode($configFileContents->username . ':' . $configFileContents->password)
                    ]
                ]
            );
            try {
                $jiraStatusCode = $response->getStatusCode();
            } catch (TransportExceptionInterface $exception) {
                $output->writeln($exception->getMessage());

                return SymfonyCommand::FAILURE;
            }
            if ($jiraStatusCode === 404) {
                $output->writeln('Task not found!');

                return SymfonyCommand::FAILURE;
            }
            if ($jiraStatusCode === 401) {
                $output->writeln('Wrong credentials!');

                return SymfonyCommand::FAILURE;
            }
            if ($jiraStatusCode !== 200) {
                $output->writeln('Something went wrong??');

                return SymfonyCommand::FAILURE;
            }
            try {
                $task = json_decode($response->getContent(), false, 512, JSON_THROW_ON_ERROR);
            } catch (Throwable $exception) {
                $output->writeln($exception->getMessage());

                return SymfonyCommand::FAILURE;
            }
            $taskTitle = $task->fields->summary;
            if (!$taskTitle) {
                $output->writeln('Something went wrong.');

                return SymfonyCommand::FAILURE;
            }
        }
        $taskTitle = str_replace(' ', '_', strtolower(preg_replace(['/[^A-Za-z\d ]/', '!\s+!'], ['', ' '], $taskTitle)));
        $branchName = $taskId . '_' . $taskTitle;
        $branchExists = true;
        $counter = 2;
        $searchBranchName = $branchName;
        while ($branchExists) {
            $gitSearchBranchProcess = new Process(['git', 'branch', '--list', $searchBranchName]);
            $gitSearchBranchProcess->run();
            if (!$gitSearchBranchProcess->getOutput()) {
                $branchExists = false;
                $branchName = $searchBranchName;
            } else {
                $searchBranchName = $branchName . '_v' . $counter;
                $counter++;
            }
        }
        (new Process(['git', 'checkout', 'staging']))->run();
        $gitPull = new Process(['git', 'pull']);
        $gitPull->run();
        if ($gitPull->getExitCode() !== 0) {
            $output->writeln($gitPull->getErrorOutput());

            return SymfonyCommand::FAILURE;
        }
        (new Process(['git', 'checkout', '-b', $branchName]))->run();

        return SymfonyCommand::SUCCESS;
    }
}
