<?php
/**
 * @copyright Copyright © Novicell ApS. All rights reserved.
 * @license   proprietary
 * @link      https://www.novicell.dk/
 */
declare(strict_types=1);

namespace Novicell;

class Application extends \Symfony\Component\Console\Application
{
    private static string $name = 'Novicell - Development tool';
    private static string $version = '1.0.0';
    private static string $logo = <<<LOGO
         _   _            _          _ _ 
        | \ | |          (_)        | | |
        |  \| | _____   ___  ___ ___| | |
        | . ` |/ _ \ \ / / |/ __/ _ \ | |
        | |\  | (_) \ V /| | (_|  __/ | |
        \_| \_/\___/ \_/ |_|\___\___|_|_|
        
LOGO;

    public function __construct(string $name = 'UNKNOWN', string $version = 'UNKNOWN')
    {
        $this->setName(self::$name);
        $this->setVersion(self::$version);
        parent::__construct($name, $version);
    }

    public function getHelp(): string
    {
        return self::$logo . parent::getHelp();
    }
}
